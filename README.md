# AgayonTodo

AgayonTodo is a [React](https://reactjs.org/) JavaScript application.
It can be used to handle small todo list. It is basic as I used this project to learn the React framework.

## Installation

Clone the repository and install it with

```bash
npm install
```

## Usage

The project must be used with [AgayonTodo](https://gitlab.com/jnanar/agayontodo), a simple go service providing an API for to-do lists. 

![Capture of AgayonTodo](capture.png)
![Capture 2 of AgayonTodo](capture2.png)
![Capture 3 of AgayonTodo](capture3.png)

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[GNU GPLv3 ](https://choosealicense.com/licenses/gpl-3.0/)

