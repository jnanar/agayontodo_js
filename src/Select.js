import React from "react";

class Select extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.selectChange = this.selectChange.bind(this);
    }

    selectChange = (event) => {
        event.stopPropagation();
        this.props.containerCallback(event.target.value);
    }

    render() {
        let options = [];
        let defaultValue = "";
        if (this.props.options && this.props.options.lenght) {
            defaultValue = this.props.options.find(it => it.id === this.props.defaultValueId).value
        }
        for (let op of this.props.options) {
            options.push(
                (<option key={op.value} id={op.id} value={op.value}>{op.name}</option>)
            )
        }
        return (
            <select key={this.props.keyVal} defaultValue={defaultValue} onChange={this.selectChange}>
                {options}
            </select>
        )
    }
}

export { Select };
