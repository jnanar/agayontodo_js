import React from 'react';
import { DeletionModal, UpdateModal } from './Common';

class ItemEditor extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = (event, itemId) => {
    this.props.containerState({ updateDialog: true, activeItem: itemId });
    event.stopPropagation();
  }

  render() {
    return (
      <button
        key={this.props.key_name}
        className='column button is-success is-size-5 is-narrow'
        el_id={this.props.ID}
        onClick={e => this.handleClick(e, this.props.itemId)}
      >
        <i className='fa fa-pencil' aria-hidden="true"></i>
      </button>

    )
  }
}

class ItemDeleter extends React.Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = (event, itemId) => {
    this.props.containerState({ deleteDialog: true, activeItem: itemId });
    event.stopPropagation();
  }

  render() {
    return (
      <button
        key={this.props.key_name}
        className='column button is-warning is-size-5 is-narrow'
        el_id={this.props.ID}
        onClick={e => this.handleClick(e, this.props.itemId)}
      >
        <i className='fa fa-times' aria-hidden="true"></i>
      </button>
    )
  }
}

class ItemCreator extends React.Component {

  constructor(props) {
    super(props);
    this.state = { updateDialog: false };
  }

  handleClick = (event) => {
    this.props.containerState({ updateDialog: true })
    event.stopPropagation();
  }

  render() {
    return (
      <div key={this.props.activeListId} className='mt-4'>
        <button
          className='button is-dark is-large is-fullwidth'
          onClick={e => this.handleClick(e)}
        >
          ADD
        </button>
      </div>
    )
  }

}

class TodoItem extends React.Component {
  constructor(props) {
    super(props);
    this.showElement = this.showElement.bind(this);
    this.state = { hidden: true, top_dropzone: false };
  }

  showElement() {
    this.setState({ hidden: !this.state.hidden });
  }

  dragOver = (event) => {
    // the title div enter the drop zone
    event.preventDefault();
    event.target.classList.add('a_expanded_dropZone');
  }

  dragLeave = (event) => {
    // the title div leaves the drop zone
    event.stopPropagation();
    event.target.classList.remove('a_expanded_dropZone');
  }

  dragStart = (event) => {
    // The title div is dragged
    event.dataTransfer.effectAllowed = 'move';
    event.dataTransfer.setData('item_id', this.props.itemId);
  }

  DragEnd = (event) => {
    event.preventDefault();
  }

  onDrop = (event, item) => {
    event.preventDefault();
    event.stopPropagation();
    // event.target contains the drop zone with parent_id : the item on top of the new one we just dropped
    // event.dataTransfer data should contains the id of the item we are moving in the drop zone
    const itemId = event.dataTransfer.getData("item_id");
    item.props.reSequence('TodoItem', parseInt(itemId), item.props.itemId);
    event.target.classList.remove('a_expanded_dropZone');
  }

  render() {
    if (this.props.topDropZone) {
      return (
        <div className="columns a_dropZone a_top_dropZone"
          parent_id={undefined}
          onDragOver={this.dragOver}
          onDrop={(event) => this.onDrop(event, this)}
          onDragLeave={this.dragLeave}
        />
      );
    } else {
      let color = 'is-info';
      if (this.props.Inactive) {
        color = 'is-dark';
      } else if (this.props.priority < 0) {
        color = 'is-white';
      } else if (this.props.priority > 1) {
        color = 'is-danger';
      } else if (this.props.priority > 0) {
        color = 'is-medium';
      }
      return (
        <li key={this.props.key_name}>
          <div>
            <div >
              <div className="columns is-mobile pt-2"
                draggable="true"
                onDragStart={this.dragStart}
                onDragEnd={this.DragEnd}
              >
                <ItemEditor
                  key_name={this.props.keyValDel}
                  itemId={this.props.itemId}
                  handleRecord={this.props.handleRecord}
                  reSequence={this.props.reSequence}
                  containerState={this.props.containerState}
                />
                <button
                  className={`column button is-size-5 a_item_title ${color}`}
                  onClick={this.showElement}
                >{this.props.name}</button>
                <ItemDeleter
                  key_name={this.props.keyValDel}
                  itemId={this.props.itemId}
                  handleRecord={this.props.handleRecord}
                  containerState={this.props.containerState}
                />
              </div>
              <div className="columns a_dropZone"
                parent_id={this.props.itemId}
                onDragOver={this.dragOver}
                onDrop={(event) => this.onDrop(event, this)}
                onDragLeave={this.dragLeave}
              />
              <div className={`columns a_TodoItem mt-1 ${this.state.hidden ? "" : "mb-2"}`}>
                <div className={`column is-1" ${this.state.hidden ? "is-hidden" : ""}`} />
                <div className={`column is-10 mb-2 is-clickable ${this.state.hidden ? "is-hidden" : ""}`}
                  onClick={this.showElement}>
                  <div className='a_display_linebreak a_overflow_wrap has-text-left is-size-6'
                  >
                    {this.props.content}
                  </div>
                </div>
                <div className={`column is-1" ${this.state.hidden ? "is-hidden" : ""}`} />
              </div>
            </div>
          </div>
        </li>
      );
    }
  }
}

class TodoItemContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { deleteDialog: false, activeItem: null, activeListId: null, searching: false };
    this.values = { updateTitle: null, updateContent: null };
  }

  containerState = (values) => {
    this.setState(values);
  }

  _setUpdateValues() {
    let title = "";
    let content = ""
    let active = true;
    let categoryListId;
    let priority = 0;
    let itemNumber = 0;
    let sequence = 0;
    const itemSequence = {};
    if (this.state.activeItem) {
      const activeItem = this.props.todoItem.filter((el) => el.ID === this.state.activeItem)[0];
      title = activeItem.Name;
      content = activeItem.Content;
      active = !activeItem.Inactive;
      categoryListId = activeItem.CategoryListId;
      priority = activeItem.Priority;
      const itemsToUse = this.props.todoItem.filter((el) => el.CategoryListId === categoryListId);
      itemsToUse.forEach((element, index) => {
        itemSequence[index] = {id: element.ID, sequence: element.Sequence};
        if (element.ID === activeItem.ID) {
          sequence = index+1;
        }
      });
      itemNumber = itemsToUse?.length
    }
    this.values = {
      updateTitle: title,
      updateContent: content,
      active: active,
      categoryListId: categoryListId,
      priority: priority,
      itemNumber: itemNumber,
      sequence: sequence,
      itemSequence: itemSequence,
    };
  }

  _getDeleteDialog() {
    let deleteDialog = null;
    if (this.state.deleteDialog) {
      deleteDialog = (
        <DeletionModal
          title="Confirmation"
          content="Are you sure you want to delete the item?"
          handleRecord={this.props.handleRecord}
          containerState={this.containerState}
          activeItem={this.state.activeItem}
          activeListId={this.props.activeListId}
        />
      )
    }
    return deleteDialog;
  }

  _getUpdateDialog() {
    let updateDialog = null;
    if (this.state.updateDialog) {
      this._setUpdateValues();
      updateDialog = (
        <UpdateModal
          handleRecord={this.props.handleRecord}
          resequence={this.props.reSequence}
          containerState={this.containerState}
          activeItem={this.state.activeItem}
          activeListId={this.props.activeListId}
          title={this.values.updateTitle}
          content={this.values.updateContent}
          active={this.values.active}
          categoryListId={this.values.categoryListId}
          priority={this.values.priority}
          todoList={this.props.todoList}
          sequence={this.values.sequence}
          itemNumber={this.values.itemNumber}
          itemSequence={this.values.itemSequence}
        />
      )
    }
    return updateDialog;
  }

  _render_item() {
    const todoItem = [];
    let itemsToUse = [];
    if (this.props.searching) {
      itemsToUse = this.props.todoItem.filter(it => this.props.searchResult.includes(it.ID));
    } else {
      if (!this.props.displayInativeItems) {
        itemsToUse = this.props.todoItem.filter(it => it.CategoryListId === this.props.activeListId && it.Inactive === false);
      } else {
        itemsToUse = this.props.todoItem.filter(it => it.CategoryListId === this.props.activeListId);
      }
    }
    const dropName = ["top_dropzone", this.props.activeListId].join("")
    todoItem.push(
      < div key={dropName} >
        <TodoItem
          key_name={["top_dropzone", this.props.activeListId].join("")}
          name={"top_dropzone"}
          handleRecord={this.props.handleRecord}
          reSequence={this.props.reSequence}
          displayModal={this.displayModal}
          hidden={false}
          containerState={this.containerState}
          topDropZone={true}
        >
        </TodoItem>
      </div >
    )
    for (let l of itemsToUse) {
      const keyValList = [l.ID, l.Type].join("");
      const keyVal = [l.ID, l.Type, 'Container'].join("");
      const keyValDel = [l.ID, l.type, 'Del'].join("");
      todoItem.push(
        <div key={keyVal}>
          <TodoItem
            Inactive={l.Inactive}
            key_name={keyValList}
            key_del={keyValDel}
            name={l.Name}
            active={l.active}
            content={l.Content}
            itemId={l.ID}
            priority={l.Priority}
            handleRecord={this.props.handleRecord}
            reSequence={this.props.reSequence}
            displayModal={this.displayModal}
            hidden={l.isHidden}
            containerState={this.containerState}
            todoList={this.props.todoList}
          >
          </TodoItem>
        </div>

      );
    }
    return todoItem;
  }

  render() {
    let deleteDialog = this._getDeleteDialog();
    let updateDialog = this._getUpdateDialog();
    return (
      <div className="todo_item is-primary is-size-4">

        <ul>{this._render_item()}</ul>
        <ItemCreator activeListId={this.props.activeListId} handleRecord={this.props.handleRecord} containerState={this.containerState} />
        {deleteDialog}
        {updateDialog}
      </div>);
  }
}

export { TodoItemContainer };
