import React from 'react';
// import ReactDOM from 'react-dom/client';
import './App.css';
import { SettingModal } from './Common';

import { TodoItemContainer } from './TodoItem';
import { TodoListContainer } from './TodoList';

const DEFAULT_API_URL = window.location.pathname + "api"

class App extends React.Component {

  constructor(props) {
    super(props);
    this.UpdateItems = this.UpdateItems.bind(this);
    this.handleRecord = this.handleRecord.bind(this);
    this.reSequence = this.reSequence.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
    this.doSearch = this.doSearch.bind(this);
    this.containerState = this.containerState.bind(this);

    this.values = {
      todoList: [],
      todoItem: [],
    };
    this.state = {
      activeListId: 0,
      itemCount: 0,
      displayLists: true,
      searchResult: [],
      displaySettings: false,
      settings: { apiUrl: DEFAULT_API_URL, user: null, displayInactive: false }
    };
    this.searchRef = React.createRef();
  }

  UpdateItems(activeListId) {
    this.setState({ activeListId: parseInt(activeListId) });
  }

  async componentDidMount() {
    await this._syncData();
  }

  async _syncData() {
    const todoList = await this._fetchLists();
    const todoItem = await this._fetchItems();
    this.values.todoList = todoList;
    this.values.todoItem = todoItem.map(v => ({ ...v, isHidden: true }));
    const listIds = todoList.map(l => l.ID);
    this.setState({ activeListId: Math.min(...listIds), itemCount: todoItem.length });
  }

  async _fetchLists() {
    const res = await this._fetchData('category_list');
    // add type item type to all the values
    const CategoryList = res.map(v => ({ ...v, Type: 'todoList' }))
    return CategoryList;
  }

  async _fetchItems() {
    const res = await this._fetchData('todo_item');
    // add type item type to all the values
    const itemList = res.map(v => ({ ...v, Type: 'item' }))
    return itemList;
  }

  async _fetchData(modelName) {
    const response = await fetch(`${this.state.settings.apiUrl}/read/${modelName}/`, { method: "GET", headers: {}, mode: 'cors' });
    if (response.status !== 200) {
      console.log('Looks like there was a problem. Status Code: ' + response.status);
      return [];
    }
    return response.json();
  }

  async containerState(values) {
    // restart the application
    const todoList = await this._fetchLists();
    const todoItem = await this._fetchItems();
    this.values.todoList = todoList;
    this.values.todoItem = todoItem.map(v => ({ ...v, isHidden: true }));
    const listIds = todoList.map(l => l.ID);
    this.setState(Object.assign({ activeListId: Math.min(...listIds), itemCount: todoItem.length }, values));
  }

  async handleRecord(actionType, recordType, defaultListId, recordId, values) {
    if (actionType === 'deletion') {
      this._deleteRecord(recordType, recordId);
    } else if (actionType === 'add') {
      await this._createRecord(recordType, Object.assign(values, { CategoryListId: defaultListId }));
    } else if (actionType === 'update') {
      const CategoryListId = values.CategoryListId || defaultListId
      await this._updateRecord(recordType, recordId, Object.assign(values, { CategoryListId: CategoryListId }));
    }

  }

  async reSequence(recordType, itemId, parentItemId) {
    await this._reSequence(recordType, itemId, parentItemId);
  }

  async _createRecord(recordType, values) {
    let modelName = "";
    if (recordType === 'TodoItem') {
      modelName = "todo_item"
    } else if (recordType === 'TodoList') {
      modelName = "category_list";
    }
    const Url = `${this.state.settings.apiUrl}/create/${modelName}`;
    const response = await fetch(
      Url,
      {
        credentials: 'include',
        method: "PUT",
        redirect: 'follow',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(values)
      }
    );
    if (response.status !== 200) {
      console.log(`Error: Create: Status Code: ${response.status} at ${Url}`);
      return;
    }
    const newItemValues = await response.json();
    this.values.todoItem.push(newItemValues);
    this.setState({})
  }

  async _updateRecord(recordType, recordId, values) {
    let records = [];
    let modelName = "";
    if (recordType === 'TodoItem') {
      records = this.values.todoItem;
      modelName = "todo_item"
    } else if (recordType === 'TodoList') {
      records = this.values.todoList;
      modelName = "category_list";
    }
    const recordIndex = records.findIndex((obj => obj.ID === recordId));
    const oldValues = records[recordIndex];
    records[recordIndex] = Object.assign(oldValues, values);
    const data = Object.assign(oldValues, values);
    const Url = `${this.state.settings.apiUrl}/update/${modelName}/${recordId}`;
    const response = await fetch(
      Url,
      {
        credentials: 'include',
        method: "POST",
        redirect: 'follow',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
    );
    if (response.status !== 200) {
      console.log(`Error: Update: Status Code: ${response.status} at ${Url}`);
      return;
    }
    this.setState({})
  }


  async _deleteRecord(recordType, recordId) {
    let tableName = "";
    if (recordType === 'TodoItem') {
      this.values.todoItem = this.values.todoItem.filter(d => d.ID !== recordId);
      tableName = "todo_items" // plurl by convention: https://gorm.io/docs/conventions.html#Pluralized-Table-Name
    } else {
      this.values.todoList = this.values.todoItem.filter(d => d.ID !== recordId);
      tableName = "category_lists"
    }
    const Url = `${this.state.settings.apiUrl}/delete/${tableName}/${recordId}`;
    const response = await fetch(
      Url,
      {
        credentials: 'include',
        method: "DELETE",
        redirect: 'follow',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
      }
    );
    if (response.status !== 200) {
      console.log(`Error: Delete: Status Code: ${response.status} at ${Url}`);
      return;
    }
    this.setState({});

  }

  async _reSequence(recordType, recordId, parentRecordId) {
    // arj todo: factorize, create an helper for these small things
    if (recordId === parentRecordId) {
      // Nothing to do
      return;
    }
    let modelName = "";
    if (recordType === 'TodoItem') {
      modelName = "todo_item"
    } else if (recordType === 'TodoList') {
      modelName = "category_list";
    }

    const Url = `${this.state.settings.apiUrl}/resequence/${modelName}`;
    const data = { record_id: recordId, parent_id: parentRecordId };
    const response = await fetch(
      Url,
      {
        credentials: 'include',
        method: "POST",
        redirect: 'follow',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
    );
    if (response.status !== 200) {
      console.log(`Error: Update: Status Code: ${response.status} at ${Url}`);
      return;
    }
    // We refetch the data in the new order
    const activeListId = this.state.activeListId;
    await this._syncData();
    this.setState({ activeListId: activeListId });

  }

  _getSettingDialog() {
    let settingDialog = null;
    if (this.state.displaySettings) {
      settingDialog = (
        <SettingModal
          containerState={this.containerState}
          settings={this.state.settings}
        />
      )
    }
    return settingDialog;
  }

  renderList() {
    if (this.state.displayLists) {
      return <TodoListContainer
        UpdateItems={this.UpdateItems}
        todoList={this.values.todoList}
        activeListId={this.state.activeListId}
      />;
    }
  }
  renderItems() {
    return <TodoItemContainer
      todoItem={this.values.todoItem}
      searching={this.state.displayLists === false}
      searchResult={this.state.searchResult}
      activeListId={this.state.activeListId}
      handleRecord={this.handleRecord}
      reSequence={this.reSequence}
      displayInativeItems={this.state.settings.displayInactive}
      todoList={this.values.todoList}
    />;
  }

  clearSearch() {
    this.setState({ displayLists: true });
    this.searchRef.current.value = ''
  }

  doSearch() {
    const todoItems = this.values.todoItem;
    const filterString = this.searchRef.current.value.toUpperCase();
    const searchResult = []
    todoItems.forEach((el, idx) => {
      if (el.Name.toUpperCase().indexOf(filterString) > -1 || el.Content.toUpperCase().indexOf(filterString) > -1) {
        searchResult.push(el.ID);
      }
    });
    this.setState({ displayLists: false, searchResult: searchResult });
  }

  render() {
    let settingDialog = this._getSettingDialog();
    return (
      <div className="App">
        <header className="App-header">
          <div className='block is-mobile a_title'>
            <div className='is-three-quarters' id="title">
              AgayonTodo
            </div>
          </div>
          <div className="a_main_box columns">
            <div className="column is-8">
              <button id="settings" className="button is-success is-flex"
                onClick={() => this.setState({ displaySettings: true })}>
                Settings
              </button>
            </div>
            <div className="column is-flex is-size-4">
              <label className='mr-1' htmlFor='search'><i className="fa fa-search" aria-hidden="true"></i></label>
              <input
                ref={this.searchRef}
                onChange={(event) => (this.doSearch())}
                id="search"
                className="a_w100"
              >
              </input>
              <button id="clear_search" className="button"
                onClick={this.clearSearch}
              >
                <i className="fa fa-times" aria-hidden="true"></i>
              </button>
            </div>
          </div>
          <div id="Main window" className="block a_main_box box">
            {settingDialog}
            <div className='pb-3'>
              {this.renderList()}
            </div>
            <div className='pb-5'>
              {this.renderItems()}
            </div>
          </div>
          <div className='subtitle is-5'>
            <a href="https://www.agayon.be" rel="noreferrer" target="_blank">Powered By AgayonTodo</a>
          </div>
        </header>
      </div>
    );
  }
}

export default App;
