import React from 'react';

class TodoList extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }


    handleClick = (event) => {
        this.props.HandleContainer(event.target.id);
        event.stopPropagation();
    }

    render() {
        return (
            <div key={this.props.key_name} className="column">
                <button
                    id={this.props.list_id}
                    className={`button is-size-4 ${this.props.active ? 'is-light' : 'is-DarkSlateBlue'}`}
                    onClick={e => this.handleClick(e)}
                >
                    {this.props.name}
                </button>
            </div>
        );
    }
}


class TodoListContainer extends React.Component {

    constructor(props) {
        super(props);
        this.HandleContainer = this.HandleContainer.bind(this);
        this.state = { activeId: this.props.activeListId };
    }

    HandleContainer = (activeId) => {
        this.props.UpdateItems(activeId);
        this.setState({ activeId: parseInt(activeId) });
    }

    _render_list() {
        const todoList = [];
        for (let l of this.props.todoList) {
            const keyValList = [l.ID, l.Type].join("");
            const keyVal = [l.ID, l.Type, 'Container'].join("");
            todoList.push(
                <TodoList
                    key={keyVal}
                    key_name={keyValList}
                    name={l.Name}
                    list_id={l.ID}
                    HandleContainer={this.HandleContainer}
                    active={this.state.activeId === l.ID}
                >
                </TodoList>
            );
        }
        return todoList;
    }

    render() {
        return (
            <div className="container todolist">
                <div className="columns is-multiline">
                    {this._render_list()}
                </div>
            </div>);
    }
}

export { TodoListContainer };
