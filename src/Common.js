import React from "react";
import { Select } from './Select';

class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick = (event, activeItem) => {
        event.stopPropagation();
    }

    _getDiscardState() {
        return {};
    }

    _getModalTitle() {
        return this.props.title;
    }

    _getModalContent() {
        return this.props.content
    }


    _getModalFooter() {
        return (
            <footer className="modal-card-foot is-block">
                <div className='columns'>
                    <div className='column'>
                        <button className='button is-large is-info'
                            onClick={(e) => this.handleClick(e, this.props.activeItem)}
                        >OK</button>
                    </div>
                    <div className='column'>
                        <button className='button is-large is-secondary'
                            onClick={(e) => this.props.containerState(this._getDiscardState())}
                        >Discard</button>
                    </div>
                </div>
            </footer>
        );
    }

    render() {
        const newId = Math.floor(Math.random() * 100);
        return (
            <div key={newId}>
                <div className='modal is-active'>
                    <div className='modal-background'></div>
                    <div className='modal-card'>
                        <div className='modal-card-head'>
                            {this._getModalTitle()}

                        </div>
                        <div className='modal-card-body'>
                            {this._getModalContent()}
                        </div>
                        {this._getModalFooter()}
                    </div>
                </div>
            </div>
        )
    }

}

class DeletionModal extends Modal {

    handleClick = (event, activeItem) => {
        event.stopPropagation();
        this.props.handleRecord('update', 'TodoItem', this.props.activeListId, activeItem, { Inactive: true });
        this.props.containerState({ deleteDialog: false, activeItem: null });
    }

    _getDiscardState() {
        return { deleteDialog: false, activeItem: null };
    }
}

class UpdateModal extends Modal {

    constructor(props) {
        super(props);
        this.state = {
            active: this.props.active,
            priority: this.props.priority,
            sequence: this.props.sequence,

        }
        const todoList = [];
        this.props.todoList.forEach(el => {
            todoList.push({ id: el.ID, name: el.Name, value: el.Name.toLowerCase().replace(" ", "_") })
        });
        this.values = {
            Name: this.props.title,
            Content: this.props.content,
            itemId: null,
            todoList: todoList,
            CategoryListId: null,
            ParentId: null,
        }
        this.handleCheckClick = this.handleCheckClick.bind(this);
        this.handleCategory = this.handleCategory.bind(this);
    }

    _getDiscardState() {
        return { updateDialog: false, activeItem: null };
    }

    handleChange(event, eventType) {
        if (eventType === 'title') {
            this.values.Name = event.target.value;
        } else if (eventType === 'content') {
            this.values.Content = event.target.value;
        } else if (eventType === 'priority') {
          this.values.priority = parseInt(event.target.value);
        } else if (eventType === 'sequence') {
            const newItemPlace = parseInt(event.target.value);
            this.values.ParentId = this.props.itemSequence[newItemPlace-1]?.id;
            if (newItemPlace === 0) {
                // New item at first position
                this.values.ParentId = -1;
            }
                       
        }
    }

    handleCheckClick = (event) => {
        event.stopPropagation();
        this.values.active = event.target.checked;
    }

    handleCategory = (value) => {
        const CategoryListId = this.values.todoList.find(it => it.value === value).id;
        this.values.CategoryListId = CategoryListId;
    }

    handleClick = (event, activeItem) => {
        event.stopPropagation();
        let action = "";
        if (activeItem) {
            action = "update";
        } else {
            // activeItem shoult be empty
            action = "add";
        }
        let active = this.state.active;
        if (this.values.active || this.values.active === false) {
            active = !!this.values.active;
        }
        let priority = this.state.priority;
        if (this.values.priority || this.values.priority === 0) {
            priority = this.values.priority;
        }
        this.props.handleRecord(action, 'TodoItem', this.props.activeListId, activeItem,
            {
                Name: this.values.Name,
                Content: this.values.Content,
                CategoryListId: this.values.CategoryListId,
                Inactive: !active,
                Priority: priority,
            }
        );
        this.props.containerState({ updateDialog: false, activeItem: null });
        if (this.values.ParentId) {
            this.props.resequence('TodoItem', this.props.activeItem, this.values.ParentId);
        }
    }

    _getModalTitle() {
        let modalTitle = ";"
        if (this.props.title) {
            modalTitle = "Update";
        } else {
            modalTitle = 'New';
        }
        return (
            <div>{modalTitle}</div>
        );

    }

    _getModalContent() {
        let defaultContent = "";
        let defaultTitle = "";
        let contentPlacehHolder = "";
        let titlePlaceHolder = "";
        if (this.props.content) {
            defaultContent = this.props.content;
        } else {
            contentPlacehHolder = 'Fill the Item content';
        }
        if (this.props.title) {
            defaultTitle = this.props.title;
        } else {
            titlePlaceHolder = "Fill the Item Title";
        }
        return (
            <div>
                <form>
                    <div className='field'>
                        <div className='label'>
                            Title
                        </div>
                        <div className='control'>
                            <input type="text" className='a_w100 a_input_title'
                                onChange={(e) => { this.handleChange(e, 'title') }}
                                placeholder={titlePlaceHolder} defaultValue={defaultTitle} />
                        </div>
                    </div>
                    <div className='field'>
                        <div className='label'>
                            Content
                        </div>
                        <div className='control'>
                            <textarea
                                className='a_w100'
                                rows="20"
                                onChange={(e) => { this.handleChange(e, 'content') }}
                                placeholder={contentPlacehHolder}
                                defaultValue={defaultContent}
                            />
                        </div>
                    </div>
                    <div className="columns">
                        <div className="column">
                            <div className='field is-horizontal'>
                                <div className='field-label is-large'>
                                    Active
                                </div>
                                <div className='field-body is-large'>
                                    <div className='control'>
                                        <input id="displayInactive" type="checkbox" className="a_state_option"
                                            onChange={this.handleCheckClick}
                                            defaultChecked={this.state.active}
                                            checked={this.values.active}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="column">
                            <div className='field is-horizontal'>
                                <div className='field-label'>
                                    Category
                                </div>
                                <div className="field-body">
                                    <div className='control'>
                                        <div className='select'>
                                            <Select
                                                keyVal={"ActivelistId" + Math.random()}
                                                defaultValueId={this.props.activeListId}
                                                options={this.values.todoList}
                                                containerCallback={this.handleCategory}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="column">
                            <div className='field is-horizontal'>
                                <div className='field-label'>
                                    Priority
                                </div>
                                <div className="field-body">
                                    <div className='control'>
                                        <input id="priority" type="range" className="a_state_option"
                                            min="-1" max="2" step="1" list="priorityValues"
                                            onChange={(e) => { this.handleChange(e, 'priority') }}
                                            placeholder='1' defaultValue={this.state.priority}
                                        />
                                        <datalist id="priorityValues">
                                            <option value="-1" label="Low"></option>
                                            <option value="0" label="Normal"></option>
                                            <option value="1" label="medium"></option>
                                            <option value="2" label="High"></option>
                                        </datalist>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="columns">
                        <div className="column">
                            <div className='field is-horizontal'>
                                <div className='field-label'>
                                    Sequence
                                </div>
                                <div className="field-body">
                                    <div className='control'>
                                        <div class="a_grid">
                                            <input id="itemNumber" type="range" className="a_state_option"
                                                min="0" max={this.props.itemNumber} step="1" list="sequenceValues"
                                                onChange={(e) => { this.handleChange(e, 'sequence') }}
                                                placeholder='1' defaultValue={this.state.sequence}
                                            />
                                            <datalist id="sequenceValues">
                                                <option value="0" label="Up"></option>
                                                <option value={this.props.itemNumber} label="Down"></option>
                                            </datalist>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class SettingModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            displayInactive: this.props.settings.displayInactive,
            apiUrl: this.props.settings.apiUrl,
        }
        this.handleCheckClick = this.handleCheckClick.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    handleCheckClick(event) {
        event.preventDefault();
        this.setState({ displayInactive: event.target.checked });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.containerState({ displaySettings: false, settings: { displayInactive: this.state.displayInactive, apiUrl: this.state.apiUrl } });
    }

    handleCancel(event) {
        event.preventDefault();
        this.props.containerState({ displaySettings: false });
    }

    render() {
        const newId = Math.floor(Math.random() * 100);
        return (
            <div key={newId}>
                <div className='modal is-active'>
                    <div className='modal-background'></div>
                    <div className='modal-card'>
                        <div className='modal-card-head'>
                            Settings
                        </div>
                        <div className='modal-card-body'>
                            <form className="box" onSubmit={this.handleSubmit}>
                                <div>
                                    <div className="field is-horizontal is-size-6">
                                        <div className="field-label">
                                            <label htmlFor="display_inactive_items" className="label">Show Inactive Items:</label>
                                        </div>
                                        <div className="field-body is-size-6">
                                            <div className="field">
                                                <input id="displayInactive" type="checkbox" className="control"
                                                    onClick={this.handleCheckClick}
                                                    defaultChecked={this.state.displayInactive}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="field is-horizontal is-size-6">
                                        <div className="field-label">
                                            <label htmlFor="api_url" className="label">API URL:</label>
                                        </div>
                                        <div className="field-body is-size-6">
                                            <div className="field">
                                                <input type="text" className="control" defaultValue={this.state.apiUrl} name="api_url" readOnly />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="mt-5 columns">
                                    <div className='column'>
                                        <button className='button is-large is-secondary'
                                            onClick={this.handleCancel}
                                        >Discard</button>
                                    </div>
                                    <div className='column'>
                                        <button className='button is-large is-primary' type="submit"
                                            onClick={this.handleSubmit}
                                        >Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export { UpdateModal, DeletionModal, SettingModal };

